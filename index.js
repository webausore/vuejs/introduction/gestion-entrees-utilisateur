let app = new Vue({
  el: "#app",

  data: {
    todos: [],
  },

  methods: {
    uppercase: function () {
      this.todos = this.todos.map((todo) => todo.toUpperCase());
    },
    lowercase: function () {
      this.todos = this.todos.map((todo) => todo.toLowerCase());
    },
  },

  beforeCreate: function () {
    fetch("https://jsonplaceholder.typicode.com/todos")
      .then((response) => response.json())
      .then((todos) => {
        todos.forEach((todo) => {
          this.todos.push(todo.title);
        });
      });
  },
});
