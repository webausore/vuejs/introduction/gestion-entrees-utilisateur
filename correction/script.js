const app = new Vue({
	el: '#app',
	data: {
		todos: [],
	},
	beforeCreate() {
		fetch('https://jsonplaceholder.typicode.com/todos')
			.then((res) => res.json())
			.then((data) => {
				data.forEach((todo) => {
					this.todos.push(todo.title);
				});
			});
	},
	methods: {
		minuscule() {
			this.todos = this.todos.map((todo) => todo.toLowerCase());
		},
		majuscule() {
			this.todos = this.todos.map((todo) => todo.toUpperCase());
		},
	},
});
