# vuejs-gestion-entrees-utilisateur

Vue JS - Attacher différentes méthodes de traitement du texte à des boutons

Preview : https://vuejs-gestion-entrees-utilisateur.netlify.app/

Consigne :

-   Pour l'utilisation de Vue, n'utilise pas vue-cli pour l'instant, inspire toi de [cette méthode](https://github.com/vuejs/vuejs.org/blob/master/src/v2/examples/vue-20-hello-world/index.html) mais en utilisant 3 fichiers séparés : index.html (import de vue via cdn) / script.js (code vuejs) / style.css (si besoin)
-   Utiliser cette api : https://jsonplaceholder.typicode.com/todos
-   Lister les différentes todos sous forme de liste ol (pas besoin de composant pour l'instant)
-   Deux boutons en haut de liste :
    -   Un pour mettre tout en minuscule
    -   Un pour mettre tout en majuscule
